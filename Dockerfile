FROM debian:bookworm

#lua5.4 luajit
RUN apt update && \
    apt install -y build-essential wget pkg-config \
    liblua5.4-dev libluajit-5.1-dev libsdl2-dev libopenal-dev libfreetype-dev libmodplug-dev libmpg123-dev libvorbis-dev libtheora-dev

RUN wget https://github.com/love2d/love/releases/download/11.4/love-11.4-linux-src.tar.gz

RUN tar -xvzf love-11.4-linux-src.tar.gz

WORKDIR ./love-11.4

RUN ./configure

RUN make

RUN cp ./src/love /usr/bin/love
RUN cp -r ./src/.libs /usr/bin/.libs

RUN mkdir /love2d

WORKDIR /love2d

ENTRYPOINT ["tail", "-f", "/dev/null"]
